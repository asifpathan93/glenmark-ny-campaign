<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Glenmark-xmas-campaign</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- css group start -->
    <?php include_once 'view/include_css.html'; ?>
    <!-- css group end -->
</head>

<body class="wh-min">
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- header start -->
    <?php include_once 'view/header.html'; ?>
    <!-- header end -->
<style>
	.swiper-step {
    width: 500px;
    height: 300px;
}

.swiper-step > .swiper-wrapper > .swiper-slide {
    background: #ddd;
    position: relative;
}

.swiper-step > .swiper-wrapper > .swiper-slide:nth-child(even) {   
    background: #ccc;
}

.swiper-set-parent {
    width: 80%;
    height: 80%;
    position: absolute;
    top: 10%;
    left: 10%;
}

.swiper-set-parent .swiper-slide {
    width: 100%;
    height: 100%;
    background: #aaa;
}

.swiper-set-parent .swiper-slide:nth-child(even) {
    background: #999;
}

.swiper-set-child {
    width: 80%;
    height: 70%;
    position: absolute;
    top: 15%;
    left: 10%;
}

.swiper-set-child .swiper-slide {
    width: 100%;
    height: 100%;
    background: #ddd;
}

.swiper-set-child .swiper-slide:nth-child(even) {
    background: #ccc;
}
</style>
    <main class="home">

        <!-- section Santagram start -->
            <section class="lyt-section typ-festive-mood">
                <div class="container">
					<div class="swiper-container swiper-step">
					<div class="swiper-wrapper">
						<div class="swiper-slide swiper-no-swiping">
							Step 1            
							<div class="swiper-container swiper-set-parent swiper-set-parent-1">
								<div class="swiper-wrapper swiper-no-swiping">
									<div class="swiper-slide swiper-no-swiping">
										Set 1.1
										<div class="swiper-container swiper-set-child swiper-set-child-1">
											<div class="swiper-wrapper">
												<div class="swiper-slide">
													Sheet 1.1.1
												</div>
												<div class="swiper-slide">
													Sheet 1.1.2
												</div>
												<div class="swiper-slide">
													Sheet 1.1.3 
												</div>
											</div>
											<div class="swiper-pagination swiper-set-child-1-pagination"></div>
										</div>
									</div>
									<div class="swiper-slide swiper-no-swiping">
										Set 1.2
										<div class="swiper-container swiper-set-child swiper-set-child-2">
											<div class="swiper-wrapper swiper-no-swiping">
												<div class="swiper-slide">
													Sheet 1.2.1
												</div>
												<div class="swiper-slide">
													Sheet 1.2.2
												</div>
												<div class="swiper-slide">
													Sheet 1.2.3 
												</div>
											</div>
											<div class="swiper-pagination swiper-set-child-2-pagination"></div>
										</div>
									</div>
									<div class="swiper-slide">
										Set 1.3 
									</div>
								</div>
								<div class="swiper-pagination swiper-set-parent-1-pagination"></div>
							</div>
						</div>
						<div class="swiper-slide swiper-no-swiping">
							Step 2
							<div class="swiper-container swiper-set-parent swiper-set-parent-2">
								<div class="swiper-wrapper swiper-no-swiping">
									<div class="swiper-slide">
										Set 2.1
									</div>
									<div class="swiper-slide">
										Set 2.2
									</div>
									<div class="swiper-slide">
										Set 2.3 
									</div>
								</div>
								<div class="swiper-pagination swiper-set-parent-2-pagination"></div>
							</div>
						</div>
						<div class="swiper-slide swiper-no-swiping">
							Step 3
						</div>
						<div class="swiper-slide swiper-no-swiping">
							Step 4
						</div>
					</div>
					<div class="swiper-pagination swiper-step-pagination"></div>
				</div>
            </section>
        <!-- section Santagram end -->

    </main>

    


    <!-- footer start -->
    <?php include_once 'view/footer.html'?>
    <!-- footer end -->

    <!-- js group start -->
    <?php include_once 'view/include_js.html'?>
	<!-- js group end -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.0.5/js/swiper.js"></script>
</body>

</html>