var winWidth, winHeight;

function get_height_width() {
    (winWidth = $(window).width()), (winHeight = $(window).height());
}

function set_height_width() {
    if ($("body").height() < winHeight) {
        $(".wh").outerHeight(winHeight);
    }
    $(".wh-min").css("min-height", winHeight);
    $(".ww").outerWidth(winWidth);
}

function set_background() {
    $(".set-bg").each(function() {
        void 0 === $(this).attr("data-mob-img") ?
            $(this).css({
                background: "url(" + $(this).attr("data-img") + ")",
                "background-size": "cover",
            }) :
            "false" == typeof $(this).attr("data-mob-img") ?
            $(window).width() > 768 &&
            $(this).css({
                background: "url(" + $(this).attr("data-img") + ")",
                "background-size": "cover",
            }) :
            $(window).width() > 768 ?
            void 0 !== $(this).attr("data-img") &&
            $(this).css({
                background: "url(" + $(this).attr("data-img") + ")",
                "background-size": "cover",
            }) :
            $(this).css({
                background: "url(" + $(this).attr("data-mob-img") + ")",
                "background-size": "cover",
            });
    });
}

function stickyMenu() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 60) {
            $(".bs-header").addClass("sticky");
        } else {
            $(".bs-header").removeClass("sticky");
        }
    });
}

function menuActive() {
    var m = $(".bs-header").attr("data-nav").toLowerCase();
    $(".main-nav li a").each(function(index) {
        if ($(this).html().toLowerCase() == m) {
            $(this).addClass("active");
        } else {
            $(this).removeClass("active");
        }
    });
}

function cheers() {
    var swiper = new Swiper(".cheer-container", {
        slidesPerView: 3,
        spaceBetween: 15,
        draggable: true,
        allowTouchMove: true,
        mousewheel: true,
        scrollbar: {
            el: ".swiper-scrollbar",
            draggable: true,
        },
        breakpoints: {
            767: {
                slidesPerView: 1.2,
            },
            992: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 3,
            },
        },
        on: {
            slideChange: function() {
                $(".cheer-container .scrolling-arrow").hide();
            },
            reachBeginning: function() {
                $(".bs-swiper.typ-joy-cheer .swiper-scrollbar-drag").removeClass("last");
            },
            reachEnd: function() {
                $(".bs-swiper.typ-joy-cheer .swiper-scrollbar-drag").addClass("last");
            },
        },
    });
}

function festive_mood() {
    var swiper = new Swiper(".festive-mood-container", {
        slidesPerView: 1,
        spaceBetween: 15,
        draggable: false,
        allowTouchMove: false,
        mousewheel: false,
        scrollbar: {
            el: ".swiper-scrollbar",
            draggable: true,
        },
        on: {
            slideChange: function() {
                $(".festive-mood-container .scrolling-arrow").hide();
            },
        },
    });
}

function animation() {
    wow = new WOW({
        boxClass: "wow", // default
        animateClass: "animated", // default
        offset: 0, // default
        mobile: false, // default
        live: true, // default
    });
    wow.init();
}

function scrollAnimations() {
    $(document).on("click", "icon.scroll", function(event) {
        event.preventDefault();
        $("html, body").animate({
                scrollTop: $($.attr(this, "href")).offset().top - 120,
            },
            1200
        );
    });
}

function open_Greenting_Modal() {
    // var baseUrl = "http://localhost:3000/glenmark-xmas-campaign/";
    var type = "";
    $(".greetinggen").click(function() {
        type = $(this).attr("id");
        $("#greentingModal").modal("show");
        var width = $(window).width();
        console.log(width);
        var height = "";
        var topFrom = "";
        var topTo = "";

        if (width < 768) {
            // console.log("less than 768");
            switch (type) {
                case "slide1":
                    topTo = 256;
                    topFrom = 314;
                    break;

                case "slide3":
                    topTo = 256;
                    topFrom = 314;
                    break;

                case "slide2":
                    topTo = 175;
                    topFrom = 231;
                    break;
            }
            height = 805;
        } else if (width >= 768 && width < 992) {
            // console.log("width less than 992 and greater than 768");
            height = 974;
            switch (type) {
                case "slide1":
                    topTo = 563;
                    topFrom = 678;
                    break;

                case "slide3":
                    topTo = 563;
                    topFrom = 678;
                    break;

                case "slide2":
                    topTo = 378;
                    topFrom = 494;
                    break;
            }
        } else if (width >= 992 && width < 1023) {
            // console.log("width less than 1025 and greater than 992");
            height = 1332;
            switch (type) {
                case "slide1":
                    topTo = 561;
                    topFrom = 674;
                    break;

                case "slide3":
                    topTo = 561;
                    topFrom = 674;
                    break;

                case "slide2":
                    topTo = 381;
                    topFrom = 496;
                    break;
            }
        } else if (width >= 1024) {
            height = 750;
            switch (type) {
                case "slide1":
                    topTo = 538;
                    topFrom = 654;
                    break;

                case "slide2":
                    topTo = 369;
                    topFrom = 479;
                    break;

                case "slide3":
                    topTo = 538;
                    topFrom = 654;
                    break;
            }
        }
        switch (type) {
            case "slide1":
                $("#greetingImg").css({ background: 'url("assets/images/big-card1.jpg")', "background-size": "cover" });
                $("#fromGreet").attr("style", "position: relative;left: 50%;top:" + topFrom + "px;color:#fce37e;width: fit-content;transform: translate(-50%);");
                $("#toGreet").attr("style", "position: relative;left: 50%;top:" + topTo + "px;color:#fce37e;width: fit-content;transform: translate(-50%);");
                break;

            case "slide2":
                $("#greetingImg").css({ background: 'url("assets/images/big-card2.jpg")', "background-size": "cover" });
                $("#fromGreet").attr("style", "position: relative;left: 50%;top:" + topFrom + "px;color: #ff0000;width: fit-content;transform: translate(-50%);");
                $("#toGreet").attr("style", "position: relative;left: 50%;top:" + topTo + "px;color: #ff0000;width: fit-content;transform: translate(-50%);");
                break;

            case "slide3":
                $("#greetingImg").css({ background: 'url("assets/images/big-card3.jpg")', "background-size": "cover" });
                $("#fromGreet").attr("style", "position: relative;left: 50%;top:" + topFrom + "px;color:#fce37e;width: fit-content;transform: translate(-50%);");
                $("#toGreet").attr("style", "position: relative;left: 50%;top:" + topTo + "px;color:#fce37e;width: fit-content;transform: translate(-50%);");
                break;
        }

        $("body").delegate("#fromGreet", "mousedown", function(event) {
            $(this).draggable({
                containment: jQuery("#greetingImg"),
            });
        });
        $("body").delegate("#toGreet", "mousedown", function(event) {
            $(this).draggable({
                containment: jQuery("#greetingImg"),
            });
        });
    });

    // download the canvas
    $("#download").click(function() {
        console.log("downloading");
        var canvas = document.createElement("div");
        canvas.id = "CursorLayer";
        canvas.width = 540;
        canvas.height = 750;
        document.body.appendChild(canvas);
        var topTo,
            topFrom = "";
        switch (type) {
            case "slide1":
                $("#CursorLayer").css({ background: 'url("assets/images/big-card1.jpg")', "background-size": "cover", position: "relative", height: "750px", width: "540px" });
                topTo = 538;
                topFrom = 654;
                break;

            case "slide2":
                $("#CursorLayer").css({ background: 'url("assets/images/big-card2.jpg")', "background-size": "cover", position: "relative", height: "750px", width: "540px" });
                topTo = 369;
                topFrom = 479;
                break;

            case "slide3":
                $("#CursorLayer").css({ background: 'url("assets/images/big-card3.jpg")', "background-size": "cover", position: "relative", height: "750px", width: "540px" });
                topTo = 538;
                topFrom = 654;
                break;
        }

        // document.getElementById("CursorLayer").style.background = "url('assets/images/greetings/greeting-card-red-pp.jpg')";
        // $("#CursorLayer").css({ background: 'url("assets/images/greetings/greeting-card-green-pp.jpg")', "background-size": "cover", position: "relative", height: "750px", width: "540px" });

        // fromm grett msg
        var fromGreet = document.createElement("p");
        fromGreet.id = "fmgreet";
        document.getElementById("CursorLayer").appendChild(fromGreet);

        var toGreet = document.createElement("p");
        toGreet.id = "togreet";
        document.getElementById("CursorLayer").appendChild(toGreet);

        switch (type) {
            case "slide1":
                $("#fmgreet").attr("style", "position: relative;font-size:22px;left: 50%;top:" + topFrom + "px;color:#fce37e;width: fit-content;transform: translate(-50%);margin:0 !important");
                $("#togreet").attr("style", "position: relative;font-size:22px;left: 50%;top:" + topTo + "px;color:#fce37e;width: fit-content;transform: translate(-50%);margin:0 !important");

                break;

            case "slide3":
                $("#fmgreet").attr("style", "position: relative;font-size:22px;left: 50%;top:" + topFrom + "px;color:#fce37e;width: fit-content;transform: translate(-50%);margin:0 !important");
                $("#togreet").attr("style", "position: relative;font-size:22px;left: 50%;top:" + topTo + "px;color:#fce37e;width: fit-content;transform: translate(-50%);margin:0 !important");

                break;

            case "slide2":
                $("#fmgreet").attr("style", "position: relative;font-size:22px;left: 50%;top:" + topFrom + "px;color:#fce37e;width: fit-content;transform: translate(-50%);margin:0 !important");
                $("#togreet").attr("style", "position: relative;font-size:22px;left: 50%;top:" + topTo + "px;color:#fce37e;width: fit-content;transform: translate(-50%);margin:0 !important");

                break;
        }
        $("#fmgreet").html($("#fromGreet").html());
        $("#togreet").html($("#toGreet").html());
        html2canvas($("#CursorLayer")).then(function(canvas) {
            var link = document.getElementById("link");
            link.setAttribute("download", "new-year-greeting-card.png");
            link.setAttribute("href", canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"));
            link.click();
            document.body.removeChild(document.getElementById("CursorLayer"));
        });
        // html2canvas($("#greetingImg"), {
        // 	onrendered: function (canvas) {
        // 		console.log("downloading", canvas);

        // 		var link = document.getElementById("link");
        // 		link.setAttribute("download", "christmas-greeting-card.png");
        // 		link.setAttribute("href", canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"));
        // 		link.click();
        // 	},
        // });
    });
}

function santagram_img() {
    var images = ["assets/images/slide1.jpg", "assets/images/slide3.jpg"];
    var current = 0;
    setInterval(function() {
        $(".center").attr("src", images[current]);
        current = current < images.length - 1 ? current + 1 : 0;
    }, 1500); /*1000 = 1 sec*/

    var current1 = 1;
    setInterval(function() {
        $(".right").attr("src", images[current1]);
        current1 = current1 < images.length - 1 ? current1 + 1 : 0;
    }, 1500); /*1000 = 1 sec*/

    var images1 = ["assets/images/slide2.jpg", "assets/images/cheer_slide1.jpg"];
    var current2 = 0;
    setInterval(function() {
        $(".left").attr("src", images1[current2]);
        current2 = current2 < images1.length - 1 ? current2 + 1 : 0;
    }, 1500); /*1000 = 1 sec*/

    var images2 = ["assets/images/gallary1.jpg", "assets/images/cheer_slide1.jpg"];
    var current3 = 0;
    setInterval(function() {
        $(".gallary1").attr("src", images2[current3]);
        current3 = current3 < images2.length - 1 ? current3 + 1 : 0;
    }, 1500); /*1000 = 1 sec*/

    var images3 = ["assets/images/gallary2.jpg", "assets/images/gallary3.jpg"];
    var current4 = 0;
    setInterval(function() {
        $(".gallary2").attr("src", images3[current4]);
        current4 = current4 < images3.length - 1 ? current4 + 1 : 0;
    }, 1500); /*1000 = 1 sec*/
}

function swiperLeftImages() {
    var swiper = new Swiper(".swiper-container-inside-slider", {
        effect: "fade",

        fadeEffect: {
            crossFade: true,
        },
        autoplay: true,
    });
}

function imagesUpload() {
    $(".iconFileup1").click(function() {
        $("#fileone").click();
        $(".iconFileup1").hide();
    });
    $(".iconFileup2").click(function() {
        $("#filetwo").click();
        $(".iconFileup2").hide();
    });
    $(".iconFileup3").click(function() {
        $("#filethree").click();
        $(".iconFileup3").hide();
    });
}

function scrollTarget(anchor, target) {
    $(anchor).click(function() {
        $("body,html").animate({ scrollTop: $(target).offset().top }, 800);
        return false;
    });
}

function setFullTrigger() {
    $(".trigger-pp").click(function() {
        var currentPop = $(this).attr("pop");
        $("#" + currentPop).modal("show");
    });
}

function masonry() {
    if ($(".grid").length > 0) {
        setTimeout(function() {
            var $container = $(".grid");
            $container.imagesLoaded(function() {
                $container.masonry({
                    itemSelector: ".masonry__item",
                });
            }, 500);
        }, 500);
    }
}

function charCount() {
    $(".photo-caption").on("keyup", function() {
        var currentLength = $(this).val().length;
        $(this)
            .next(".charline")
            .find(".char")
            .text(100 - currentLength);
        console.log(100 - currentLength);
    });
}

$(window).resize(function() {
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function() {
        set_background();
    }, 250);
});

$(window).load(function() {
    $(".loader").hide();
    masonry();
    swiperLeftImages();
});

$(function() {
    get_height_width();
    set_height_width();
    set_background();
    cheers();
    animation();
    festive_mood();
    scrollAnimations();
    open_Greenting_Modal();
    //swiperLeftImages();
    imagesUpload();
    setFullTrigger();
    charCount();
    scrollTarget(".bs-banner .inner-content i", "#santasleigh");
    scrollTarget(".video-play", ".typ-jukebox");
});